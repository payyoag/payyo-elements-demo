FROM php:7.4-apache

RUN a2enmod rewrite

COPY . /app
RUN rmdir /var/www/html && ln -s /app/public /var/www/html