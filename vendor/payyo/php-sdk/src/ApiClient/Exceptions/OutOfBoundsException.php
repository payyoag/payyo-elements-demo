<?php

namespace TrekkPay\Sdk\ApiClient\Exceptions;

class OutOfBoundsException extends Exception
{
}
