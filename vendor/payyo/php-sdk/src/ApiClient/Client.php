<?php
namespace TrekkPay\Sdk\ApiClient;

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\Cache;
use GuzzleHttp\Psr7\Uri;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use TrekkPay\Sdk\ApiClient\Http\Client as HttpClient;
use TrekkPay\Sdk\ApiClient\Http\Guzzle6Client;
use TrekkPay\Sdk\ApiClient\Http\Request;
use TrekkPay\Sdk\ApiClient\Http\Response;
use TrekkPay\Sdk\ApiClient\Methods\Accounting;
use TrekkPay\Sdk\ApiClient\Methods\PaymentPage;
use TrekkPay\Sdk\ApiClient\Methods\Transaction;
use TrekkPay\Sdk\ApiClient\Methods\Transactions;

class Client
{
    /** @var string */
    private $baseUrl = 'https://api.trekkpay.io';
    /** @var string */
    private $version = '2';
    /** @var Credentials */
    private $credentials;
    /** @var HttpClient */
    private $httpClient;
    /** @var Logger */
    private $logger;
    /** @var Cache */
    private $cache;
    /** @var bool */
    private $useCache = false;
    /** @var int */
    private $cacheTtl = 30;
    /** @var null|string */
    private $correlationId;
    /** @var array */
    private $meta = [];
    /** @var null|string */
    private $metaHash;
    
    public static function newCachingClient(
        Credentials $credentials,
        Cache $cache,
        int $cacheTtl,
        LoggerInterface $logger = null,
        HttpClient $httpClient = null
    ): self {
        return (new static($credentials, $logger, $httpClient))->withCache($cacheTtl, $cache);
    }
    
    public function __construct(Credentials $credentials, LoggerInterface $logger = null, HttpClient $httpClient = null)
    {
        $this->credentials = $credentials;
        $this->logger = $logger instanceof Logger ? $logger : new Logger($logger ?: new NullLogger());
        $this->httpClient = $httpClient ?: new Guzzle6Client();
        $this->cache = new ArrayCache();
    }

    /**
     * We don't clone the cache as it may have been referenced from the outside to gatter information.
     */
    public function __clone()
    {
        $this->credentials = clone $this->credentials;
        $this->httpClient = clone $this->httpClient;
        $this->logger = clone $this->logger;
    }
    
    public function withNewCredentials(Credentials $credentials): self
    {
        $clone = clone $this;
        $clone->credentials = $credentials;

        return $clone;
    }
    
    public function withVersion(string $version): self
    {
        $clone = clone $this;
        $clone->version = $version;

        return $clone;
    }

    public function withOtherBaseUrl(string $baseUrl): self
    {
        $clone = clone $this;
        $clone->baseUrl = $baseUrl;

        return $clone;
    }

    public function withCorrelationId(string $correlationId): self
    {
        $clone = clone $this;
        $clone->correlationId = $correlationId;

        return $clone;
    }
    
    public function withMeta(array $meta, string $hash): self
    {
        if (count($meta) === 0 || empty($hash)) {
            return $this;
        }

        $clone = clone $this;
        $clone->meta = $meta;
        $clone->metaHash = $hash;

        return $clone;
    }

    public function getCache(): Cache
    {
        return $this->cache;
    }

    /**
     * @param int|bool|null $ttl
     * @param Cache|null $cache
     * @return $this
     */
    public function withCache($ttl = null, ?Cache $cache = null): self
    {
        $clone = clone $this;
        if ($cache) {
            $clone->cache = $cache;
        }
        
        if (false === $ttl) {
            $clone->useCache = false;
        } else {
            $clone->useCache = true;

            if (null !== $ttl) {
                $clone->cacheTtl = $ttl;
            }
        }
        
        return $clone;
    }

    public function transaction(): Transaction
    {
        return new Transaction($this);
    }

    public function transactions(): Transactions
    {
        return new Transactions($this);
    }

    public function accounting(): Accounting
    {
        return new Accounting($this);
    }
    
    public function paymentPage(): PaymentPage
    {
        return new PaymentPage($this);
    }
    
    public function request(string $method, array $params = []): Response
    {
        $cacheKey = $this->generateCacheKey($method, $params);

        if (count($this->meta) > 0 && !empty($this->metaHash)) {
            $params['@meta'] = $this->meta;
            $params['@meta_hash'] = $this->metaHash;
        }
        
        $data = \GuzzleHttp\json_encode([
            'jsonrpc' => '2.0',
            'method' => $method,
            'params' => new \ArrayObject($params),
            'id' => 1,
        ]);

        if ($this->useCache && $response = $this->cache->fetch($cacheKey)) {
            return $response;
        }

        $request = new Request('POST', '/v' . $this->version);
        $request->getBody()->write($data);
        $request->getBody()->rewind();

        $response = $this->doRequest($request);

        if ($this->useCache) {
            $response->setCachedSince(new \DateTimeImmutable());
            $this->cache->save($cacheKey, $response, $this->cacheTtl);
        }

        return $response;
    }

    private function generateCacheKey(string $method, array $params): string
    {
        $data = \GuzzleHttp\json_encode([
            'method' => $method,
            'params' => new \ArrayObject($params),
        ]);

        return md5($this->credentials->getPublicKey().':'.$data);
    }
    
    private function doRequest(Request $request): Response
    {
        $request->setLogger($this->logger);

        $uri = new Uri($this->baseUrl.$request->getRequestTarget());

        $this->logger->info("Request: {$request->getMethod()} {$uri}");

        /** @var Request $request */
        $request = $request->withAuthorizationHeader($this->credentials);
        $request = $request->withUri($uri);
        $request = $request->withMethod('post');
        $request = $request->withHeader('Accept', 'application/json');
        $request = $request->withHeader('Content-Type', 'application/json');

        if (null !== $this->correlationId) {
            $request = $request->withHeader('X-Correlation-ID', $this->correlationId);
        }

        $response = Response::fromPsr7($this->httpClient->request($request));

        try {
            $response->assertSuccessful();
        } catch (RequestError $e) {
            $this->logger->logRequestError($e);

            throw $e;
        }

        return $response;
    }
}
