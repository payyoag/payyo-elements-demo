<?php
namespace TrekkPay\Sdk\ApiClient;

use TrekkPay\Sdk\ApiClient\Exceptions\RuntimeException;

class RequestError extends RuntimeException
{
    /** @var int */
    private $statusCode = 0;
    /** @var array */
    private $details = [];
    /** @var array */
    private $debugInfo = [];

    public static function create(
        string $errorMessage,
        int $errorCode,
        int $statusCode,
        array $details,
        array $debugInfo = []
    ): self {
        $e = new static($errorMessage, $errorCode);
        $e->statusCode = $statusCode;
        $e->details = $details;
        $e->debugInfo = $debugInfo;

        return $e;
    }
    
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getDetails(): array
    {
        return $this->details;
    }

    public function getDebugInfo(): array
    {
        return $this->debugInfo;
    }
}
