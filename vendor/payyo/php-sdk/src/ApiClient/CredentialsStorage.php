<?php
namespace TrekkPay\Sdk\ApiClient;

use Doctrine\Common\Cache\Cache;
use TrekkPay\Sdk\ApiClient\Exceptions\OutOfBoundsException;

final class CredentialsStorage
{
    /** @var Cache */
    private $store;

    public function __construct(Cache $store)
    {
        $this->store = $store;
    }

    public function store(string $key, Credentials $credentials): void
    {
        $this->store->save($key, $credentials, 0);
    }
    
    public function find(string $key): Credentials
    {
        $credentials = $this->store->fetch($key);
        if (!$credentials) {
            throw new OutOfBoundsException("No credentials for key '{$key}' available.");
        }

        return $credentials;
    }
}
