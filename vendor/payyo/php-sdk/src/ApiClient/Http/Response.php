<?php
namespace TrekkPay\Sdk\ApiClient\Http;

use TrekkPay\Sdk\ApiClient\RequestError;
use TrekkPay\Sdk\ApiClient\Exceptions\OutOfBoundsException;
use TrekkPay\Sdk\ApiClient\Exceptions\RuntimeException;
use Psr\Http\Message\ResponseInterface as Psr7Response;

final class Response extends \GuzzleHttp\Psr7\Response
{
    /** @var array|null */
    private $data;
    /** @var \DateTimeImmutable|null */
    private $cachedSince;

    public static function fromPsr7(Psr7Response $response): self
    {
        return new static(
            $response->getStatusCode(),
            $response->getHeaders(),
            $response->getBody()
        );
    }

    public function setCachedSince(\DateTimeImmutable $cachedSince): void
    {
        $this->cachedSince = $cachedSince;
    }

    public function getCachedSince(): ?\DateTimeImmutable
    {
        return $this->cachedSince;
    }

    /**
     * @throws ConnectionError
     * @throws RequestError
     */
    public function assertSuccessful(): void
    {
        $statusCode = $this->getStatusCode();

        if ($statusCode >= 200 && $statusCode < 300) {
            if (!$this->hasValue('error')) {
                return;
            }

            throw RequestError::create(
                $this->getValue('error.message'),
                $this->getValue('error.code'),
                $statusCode,
                (array) $this->getValueWithDefault('error.data', [])
            );
        }

        throw new ConnectionError(
            'JSON-RPC request failed. Got status code '.$statusCode,
            $statusCode
        );
    }

    /**
     * @throws RuntimeException
     */
    public function getValues(): array
    {
        if (null === $this->data) {
            $body = $this->getBody()->getContents();

            if ('' === $body) {
                $data = [];
            } else {
                $data = json_decode($body, true);
                if (!is_array($data)) {
                    throw new RuntimeException('Failed to JSON decode server response: '.json_last_error_msg().'. Response body: '.$body);
                }
            }

            $this->data = $data;
        }

        return $this->data;
    }
    
    public function getValue(string $key, string $separator = '.')
    {
        return $this->getValueRecursively(explode($separator, $key), $this->getValues());
    }

    public function getValueWithDefault(string $key, $default, string $separator = '.')
    {
        try {
            return $this->getValue($key, $separator);
        } catch (OutOfBoundsException $e) {
            return $default;
        }
    }
    
    public function hasValue(string $key, $separator = '.'): bool
    {
        try {
            $this->getValue($key, $separator);

            return true;
        } catch (OutOfBoundsException $e) {
            return false;
        }
    }

    /**
     * @throws OutOfBoundsException
     */
    private function getValueRecursively(array $keys, array $data)
    {
        $key = array_shift($keys);

        if (array_key_exists($key, $data)) {
            if (0 === count($keys)) {
                return $data[$key];
            }

            return $this->getValueRecursively($keys, $data[$key]);
        }

        throw new OutOfBoundsException("Could not find key '$key' in data: ".print_r($data, true));
    }
}
