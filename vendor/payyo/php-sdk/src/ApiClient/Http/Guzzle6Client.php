<?php
namespace TrekkPay\Sdk\ApiClient\Http;

use GuzzleHttp\Client as GuzzleClient;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class Guzzle6Client implements Client
{
    /** @var GuzzleClient */
    private $client;

    private function getClient(): GuzzleClient
    {
        if (!$this->client) {
            $this->client = new GuzzleClient([
                'timeout' => 300,
                'verify' => false,
                'http_errors' => false,
            ]);
        }

        return $this->client;
    }

    public function request(Request $request): Response
    {
        return $this->getClient()->send($request);
    }
}
