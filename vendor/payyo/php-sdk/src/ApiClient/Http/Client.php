<?php
namespace TrekkPay\Sdk\ApiClient\Http;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

interface Client
{
    public function request(Request $request): Response;
}
