<?php

namespace TrekkPay\Sdk\ApiClient\Methods;

use TrekkPay\Sdk\ApiClient\Http\Response;

final class Accounting extends MethodsCollection
{
    /**
     * @param int|int[] $merchantIds
     * @param array     $params
     *
     * @return Response
     */
    public function searchAccounts($merchantIds, array $params = [])
    {
        $params['merchant_ids'] = (array) $merchantIds;

        return $this->request('journal.searchAccounts', $params);
    }

    /**
     * @param int   $merchantId
     * @param array $params
     *
     * @return Response
     */
    public function listPayoutStatementsForMerchant($merchantId, array $params = [])
    {
        $params['merchant_ids'] = [(int) $merchantId];

        return $this->request('payoutStatements.search', $params);
    }
}
