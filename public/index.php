<?php
declare(strict_types=1);

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use TrekkPay\Sdk\ApiClient\Client;
use TrekkPay\Sdk\ApiClient\Credentials;
use TrekkPay\Sdk\ApiClient\RequestError;

require __DIR__ . '/../vendor/autoload.php';

session_start();

define('ENDPOINT', getenv('ENDPOINT'));
define('MERCHANT_ID', (int) getenv('MERCHANT_ID'));
define('API_KEY', getenv('API_KEY'));
define('SECRET_KEY', getenv('SECRET_KEY'));

$app = new \Slim\App([
    'flash' => function () {
        return new \Slim\Flash\Messages();
    },
    'view' => function (Container $container) {
        $view = new \Slim\Views\Twig(__DIR__ . '/../templates');
    
        $router = $container->get('router');
        $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
        $view->addExtension(new Slim\Views\TwigExtension($router, $uri));
        
        $view['endpoint'] = ENDPOINT;
        $view['merchant_id'] = MERCHANT_ID;
        $view['flash_messages'] = $container->get('flash')->getMessages();
        
        return $view;
    },
    'settings' => [
        'displayErrorDetails' => true,
    ],
]);

// API client, CORS, etc.
$app->add(function (Request $request, Response $response, callable $next) {
    $client = (new Client(new Credentials(API_KEY, SECRET_KEY)))->withOtherBaseUrl('https://api.' . ENDPOINT);
    $request = $request->withAttribute('api_client', $client);
    
    /** @var Response $response */
    $response = $next($request, $response);
    
    return $response->withHeader('Access-Control-Allow-Origin', '*');
});

$app->get('/', function(Request $request, Response $response) {
    return $this->view->render($response, 'booking.twig');
});

$app->get('/pay', function(Request $request, Response $response) {
    $params = $request->getQueryParams();
    
    if (empty($params['reference']) || empty($params['amount']) || empty($params['currency'])) {
        return $response->withRedirect('/');
    }
    
    /** @var Client $client */
    $client = $request->getAttribute('api_client');
    
    if (!empty($params['alias_id'])) {
        try {
            $details = [
                'merchant_id' => MERCHANT_ID,
                'merchant_reference' => $params['reference'],
                'amount' => (int) $params['amount'],
                'currency' => $params['currency'],
                'funding_instrument' => [
                    'type' => 'credit_card_alias',
                    'alias_id' => $params['alias_id'],
                ],
                'was_dcc_offered' => true,
                'return_urls' => [
                    'success' => (string) $request->getUri()->withPath('/3ds')->withQuery('success=1'),
                    'fail' => (string) $request->getUri()->withPath('/3ds')->withQuery('success=0'),
                ],
            ];
            if (!empty($params['dcc_quote_reference'])) {
                $details['dcc_quote_reference'] = $params['dcc_quote_reference'];
            }
            
            $initiateResult = $client->request('transaction.initiate', $details);
            $transactionId = $initiateResult->getValue('result.transaction_id');
            
            switch ($initiateResult->getValue('result.next_action')) {
                case 'redirect':
                    return $response->withRedirect($initiateResult->getValue('result.redirect_url'));
                case 'capture':
                    $client->request('transaction.capture', ['transaction_id' => $transactionId]);
                    break;
            }
        
            return $response->withRedirect('/paid?transaction_id=' . $transactionId);
        } catch (RequestError $e) {
            $this->flash->addMessage('errors', $e->getMessage());
            
            return $response->withRedirect('/pay?' . http_build_query([
                'reference' => $params['reference'],
                'amount' => $params['amount'],
                'currency' => $params['currency'],
            ]));
        }
    }
    
    $reservationId = $client->request('vault.makeReservation', [
        'merchant_id' => MERCHANT_ID,
    ])->getValue('result.reservation_id');
    
    return $this->view->render($response, 'payment.twig', [
        'reference' => $params['reference'],
        'amount' => $params['amount'],
        'currency' => $params['currency'],
        'reservation_id' => $reservationId,
    ]);
})->setName('payment');

$app->get('/3ds', function(Request $request, Response $response) {
    $params = $request->getQueryParams();
    
    if (empty($params['transaction_id']) || !isset($params['success'])) {
        return $response->withRedirect('/');
    }
    
    /** @var Client $client */
    $client = $request->getAttribute('api_client');
    
    if ($params['success']) {
        $client->request('transaction.capture', ['transaction_id' => $params['transaction_id']]);
        
        return $response->withRedirect('/paid?transaction_id=' . $params['transaction_id']);
    }
    
    $client->request('transaction.abort', ['transaction_id' => $params['transaction_id']]);
    $this->flash->addMessage('errors', '3DS authentication failed');
    
    return $response->withRedirect('/');
});

$app->get('/paid', function(Request $request, Response $response) {
    $params = $request->getQueryParams();
    
    if (empty($params['transaction_id'])) {
        return $response->withRedirect('/');
    }
    
    /** @var Client $client */
    $client = $request->getAttribute('api_client');
    
    try {
        $details = $client->request('transaction.getDetails', [
            'transaction_id' => $params['transaction_id'],
        ])->getValue('result');
    } catch (RequestError $e) {
        $details = [];
        $this->flash->addMessage('errors', $e->getMessage());
    }
    
    return $this->view->render($response, 'details.twig', [
        'details' => $details,
    ]);
});

$app->get('/dcc-quote.json', function(Request $request, Response $response) {
    $params = $request->getQueryParams();
    
    if (empty($params['iin']) || empty($params['amount']) || empty($params['currency'])) {
        return $response->withJson([], 500);
    }
    
    /** @var Client $client */
    $client = $request->getAttribute('api_client');
    
    try {
        $details = $client->request('transaction.getDccQuote', [
            'merchant_id' => MERCHANT_ID,
            'amount' => (int) $params['amount'],
            'currency' => $params['currency'],
            'funding_instrument' => [
                'type' => 'credit_card',
                'number' => $params['iin'] . ' 34 1234 1234',
                'holder' => 'Mr Quote',
                'expires' => (date('Y') + 5) . '-01',
            ],
        ])->getValue('result');
    
        return $response->withJson($details);
    } catch (RequestError $e) {
        return $response->withJson(['error' => $e->getMessage()], 403);
    }
})->setName('dcc_quote');

$app->run();