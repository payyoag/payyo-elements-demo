# Payyo "Elements" Demo

This is a hacky proof-of-concept that we can use our vault.js to authorize transactions without the cardholder data touching the integrator's server.

## Notes

To make Docker the only requirement and keep things simple, the `vendor` directory is under version control.

## Usage

1. Copy `docker-compose.yml.dist` to `docker-compose.yml`
2. Update the environment variables in `docker-compose.yml`
3. Run `docker-compose up -d`
4. Browse to [http://lvh.me:8686/](http://lvh.me:8686/) (lvh.me just points to localhost and helps avoid some CORS issues)
